#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ArduCAM.h>
#include <SPI.h>
#include <Wire.h>

//setting the buffer size. This can be changed to help laggy streaming
static const size_t buffer1 = 4096;

//setting the ip address of the web server
IPAddress ipAdd(192,168,1,117);
IPAddress gateway(192,168,1,1);
IPAddress subnet(255,255,255,0);

const int CS = 16;

//Setting the connection ssid and password
const char *ssid = "PLUSNET-JQ95";
const char *pass = "4c3b9ed979";

//creating the camera object
ArduCAM doorCam(OV2640, CS);

//creating the ESP8266 webserver for the module to connect to
ESP8266WebServer server(80);

//Start the capture of the camera
void start_capture(){
  doorCam.clear_fifo_flag();
  doorCam.start_capture();
}

//Sending the camera image to the web server
void streamserver(){
  
  WiFiClient client = server.client();

  String response = "HTTP/1.1 200 OK\r\n";
  response += "Content-Type: multipart/x-mixed-replace; boundary=frame\r\n\r\n";
  server.sendContent(response);

  //only send the information when the client is connected
  while(client.connected()){
    start_capture();

    while(!doorCam.get_bit(ARDUCHIP_TRIG, CAP_DONE_MASK));

    size_t leng = doorCam.read_fifo_length();

    doorCam.CS_LOW();
    doorCam.set_fifo_burst();

    #if !(defined (ARDUCAM_SHIELD_V2) && defined (OV2640_CAM))
      SPI.transfer(0xFF);
    #endif
    if(!client.connected()) break;
    response = "--frame\r\n";
    response += "Content-type: image/jpeg\r\n\r\n";
    server.sendContent(response);

    static uint8_t buffer[buffer1] = {0xFF};

    while (leng) {
      size_t willCopy = (leng < buffer1) ? leng : buffer1;
      SPI.transferBytes(&buffer[0], &buffer[0], willCopy);
      if(!client.connected()) break;
      client.write(&buffer[0], willCopy);
      leng -= willCopy;
    }
    doorCam.CS_HIGH();

    if(!client.connected()) break;
  }
}

//set up function
void setup(){

    Wire.begin();


  //Starting the Arducam
  Serial.println("Starting Front Door Camera...");
  Serial.begin(115200);
  delay(10);

  //sets the chip select as an output
  pinMode(CS, OUTPUT);

  SPI.begin();

  //set the camera to display JPEG
  doorCam.set_format(JPEG);
  //start the camera
  doorCam.InitCAM();

  //Set the Wifi mode and the connect to the wifi
  WiFi.mode(WIFI_STA);

  WiFi.begin(ssid, pass);
  Serial.print("Connecting to ");
  Serial.print(ssid); Serial.println(" ...");

  
  int i = 1;
  while(WiFi.status() != WL_CONNECTED){
    delay(500);
    Serial.print(i++); Serial.print(' ');
  }
  Serial.println();
  Serial.println("Connection Established");
  Serial.print("IP address: ");
  Serial.println(ipAdd);
  
  WiFi.config(ipAdd, gateway, subnet);

  //Start the server
  server.on("/frontdoor", HTTP_GET, streamserver);
  server.begin();
  Serial.println("Starting Server... ");
  delay(1000);
  Serial.println("Server Started.");

}

//loop function
void loop(){
  //handle incoming client requests
  server.handleClient();
}
